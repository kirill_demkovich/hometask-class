﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{ 
    class Program
    {
        static void Main(string[] args)
        {
              Bank bank = new Bank();
              Client client = new Client();
            byte res = 0;
            Console.WriteLine($"Добро пожаловать в наш банк! Давайте создаим ему имя и укажем город!");
            bank.Name = Console.ReadLine();
            bank.Adress = Console.ReadLine();
            Start();

            void Start()
            {
                Console.WriteLine($"Отлично! Мы открыли {bank.Name} в городе: {bank.Adress}А теперь я предлагаю выбрать, что нам теперь делать! \n " +
                    "Введите 1 - Добавить клиента \n" +
                    "Введите 2 - Удалить клиента \n" +
                    "Введите 3 - Изменить баланс клиенту \n" +
                    "Введите 4 - Показать всех клиентов \n" +
                    "Введите 5 - Найти клиента по имени \n" +
                    "Введите 6 - Метод для изменения имени или фамилии \n" +
                    "Введите 7 - чтобы выйти");
                do
                {
                    Console.WriteLine("Введите значение от 1 до 7");
                    res = Convert.ToByte(Console.ReadLine());
                } while (res < 1 || res > 7);
                switch (res)
                {
                    case 1:
                        bank.AddClient();
                        break;
                    case 2:
                        bank.RemoveClient();
                        break;
                    case 3:
                        bank.ChangeBalance();
                        break;
                    case 4:
                        bank.ShowAllClient();
                        break;
                    case 5:
                        bank.ShowClientName();
                        break;
                    case 6:
                        bank.ChangeClient();
                        break;
                    case 7:
                        Environment.Exit(0);
                        break;
                }
                Start();
            }
        }
    }
}
