﻿using System;

    public class Client
    {
        public string Name;
        public string Surname;
        public byte Age;
        public decimal Cash;

        public Client()
        {
            Name = "name";
            Surname = "surname";
            Age = 20;
            Cash = 100.00m;
        }

        public Client(string name, string surname, byte age, decimal cash)
        {
            Name = name;
            Surname = surname;
            Age = age;
            Cash = cash;
        }

        public void ChangeName()
        {
            Name = Console.ReadLine();
        }

        public void ChangeSurname()
        {
            Surname = Console.ReadLine();
        }
        public void ShowClient()
        {
            Console.WriteLine($"Name: {Name} \n Surname: {Surname} \n Age: {Age}");
        }
        public void ChangeCash()
        {
        Cash = Convert.ToDecimal(Console.ReadLine()); 
        }
}
