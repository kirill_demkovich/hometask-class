﻿using System;
using System.Collections.Generic;

class Bank
{
    string answer;
    string person;
    public string Name;
    public string Adress;
    //конструктор по умолчанию
    public Bank()
    {
        Name = "name";
        Adress = "city";
    }
    // конструктор с параметрами
    public Bank(string name, string adress)
    {
        Name = name;
        Adress = adress;
    }


    //создаю список клинтов
    List<Client> clientsList = new List<Client>();
   // Client client = new Client();
    public void AddClient()
    {
        Console.WriteLine("Введите количество новых клиентов: ");
        int count = Convert.ToInt32(Console.ReadLine());

        for (int i = 0; i < count; i++)
        {
            Console.WriteLine("Введите имя:");
            string name = Console.ReadLine();
            Console.WriteLine("Введите фамилию:");
            string surname = Console.ReadLine();
            Console.WriteLine("Введите возраст:");
            byte age = Convert.ToByte(Console.ReadLine());
            Console.WriteLine("Введите текущий баланс клиента:");
            decimal cash = Convert.ToDecimal(Console.ReadLine());

            Client client = new Client(name, surname, age, cash);

            clientsList.Add(client);
        }
    }
    public void ShowAllClient()
    {
        for (int i = 0; i < clientsList.Count; i++)
        {
            clientsList[i].ShowClient();
            Console.WriteLine("клиентик");
        }
    }
    public void ShowClientName()
    {

        Console.WriteLine("Кого хотите найти?");
        person = Console.ReadLine();
        for (int i = 0; i < clientsList.Count; i++)
        {
            if (person == clientsList[i].Name)
            {
                clientsList[i].ShowClient();
            }
        }
        Console.WriteLine("Хотите посмотреть кого-то ещё?? Ответьте да или нет.");
        answer = Console.ReadLine();
        if (answer == "да")
        {
            ShowClientName();
        }
    }

    public void RemoveClient()
    {
        string remover;
        Console.WriteLine("Кого хотите удалить?");
        for (int i = 0; i < clientsList.Count; i++)
        {
            remover = clientsList[i].Surname;
            Console.Write($"{remover}; ");
        }

        remover = Console.ReadLine();
        for (int i = 0; i < clientsList.Count; i++)
        {
            if (remover == clientsList[i].Surname)
            {
                Console.WriteLine($"Клиент {clientsList[i].Surname} удалён");
                clientsList.RemoveAt(i);
            }
        }
        Console.WriteLine("Хотите удалить кого-то ещё?? Ответьте да или нет.");
        answer = Console.ReadLine();
        if (answer == "да")
        {
            RemoveClient();
        }

    }
    public void ChangeBalance()
    {
            Console.WriteLine("Кому хотите поменять баланс?");
            for (int i = 0; i < clientsList.Count; i++)
            {
                person = $"{clientsList[i].Name} - с балансом {clientsList[i].Cash} ";
                Console.WriteLine($"{person}; ");
            }

        person = Console.ReadLine();
            for (int i = 0; i < clientsList.Count; i++)
            {
                if (person == clientsList[i].Name)
                {
                    Console.WriteLine($"Какой актуальный баланс у клиента {clientsList[i].Name}?");
            //        clientsList[i].Cash = Convert.ToDecimal(Console.ReadLine());
                clientsList[i].ChangeCash();
                }
            }
            Console.WriteLine("Хотите дать денег кому-то ещё?? Ответьте да или нет.");
            answer = Console.ReadLine();
            if (answer == "да")
            {
                ChangeBalance();
            }

     }
    public void ChangeClient()
    {
        int w;
        Console.WriteLine("Кому хотите поменять ФИО? Укажите номер");
        for (int i = 0; i < clientsList.Count; i++)
        {
            person = $"{i}. {clientsList[i].Name} {clientsList[i].Surname}";
            Console.WriteLine($"{person}; ");
        }
        int numberofclient = Convert.ToInt32(Console.ReadLine());
        do
        {
        Console.WriteLine("Нажмите 1 - если хотите поменять имя \n" +
            "Нажмите 2 - чтобы поменять фамилию \n" +
            "Нажмите 3 - чтобы отменить");
            w = Convert.ToInt32(Console.ReadLine());
        } while (w < 1 || w > 3);
        switch (w)
        {
            case 1:
                Console.WriteLine("Введите новое имя");
                clientsList[numberofclient].ChangeName();
                break;
            case 2:
                Console.WriteLine("Введите новую фамилию");
                clientsList[numberofclient].ChangeSurname();
                break;
            case 3:
                break;
        }
        Console.WriteLine("Хотите поменять ФИО кому-то ещё?? Ответьте да или нет.");
        answer = Console.ReadLine();
        if (answer == "да")
        {
            ChangeClient();
        }

    }

}  